|                 	| |
|-------------------------	|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|
| Pipeline                	| [![pipeline status](https://gitlab.com/Prophet731/battlefield-admin-control-panel/badges/v3.0/pipeline.svg)](https://gitlab.com/Prophet731/battlefield-admin-control-panel/commits/v3.0) 	|
| Coverage                	| [![coverage report](https://gitlab.com/Prophet731/battlefield-admin-control-panel/badges/v3.0/coverage.svg)](https://gitlab.com/Prophet731/battlefield-admin-control-panel/commits/v3.0) 	|
| Latest Stable Version   	| [![Latest Stable Version](https://poser.pugx.org/adkats/bfacp/v/stable)](https://packagist.org/packages/adkats/bfacp)                                                                    	|
| Latest Unstable Version 	| [![Latest Unstable Version](https://poser.pugx.org/adkats/bfacp/v/unstable)](https://packagist.org/packages/adkats/bfacp)                                                                	|
| License                 	| [![License](https://poser.pugx.org/adkats/bfacp/license)](https://packagist.org/packages/adkats/bfacp)                                                                                 	|
| Monthly Downloads       	| [![Monthly Downloads](https://poser.pugx.org/adkats/bfacp/d/monthly)](https://packagist.org/packages/adkats/bfacp)                                                                       	|
| Composer Lock           	| [![composer.lock](https://poser.pugx.org/adkats/bfacp/composerlock)](https://packagist.org/packages/adkats/bfacp)                                                                        	|
| Dependencies              | [![Depfu](https://badges.depfu.com/badges/bf97a835092b4548e0e4db94f4fc4fb0/count.svg)](https://depfu.com/gitlab/Prophet731/battlefield-admin-control-panel?project_id=10110)              |

# Battlefield Admin Control Panel

The Battlefield Admin Control Panel (BFACP) is a web based application
designed to interact with the plugin
[AdKats](https://myrcon.net/topic/153-free-advanced-in-game-admin-and-ban-enforcer-adkats-7500/)
for [ProCon](https://myrcon.net).

It's purpose is it make administrating your server with the AdKats
plugin easier.

This project was started back in 2013 at the end of life of Battlefield
3 and didn't fully become what it until Battlefield 4. What started as a
tool for just internal use at my former gaming community (ADK Gamers)
turned into something that could benefit many gaming communities that
used the AdKats plugin.
