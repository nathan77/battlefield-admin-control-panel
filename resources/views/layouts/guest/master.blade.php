<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="icon" href="{{ asset('frontend/images/favicon.ico') }}">
    <title>@yield('title') - {{ config('bfacp.community_name', 'Battlefield Admin Control Panel') }}</title>
    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/font-icons/entypo/css/entypo.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/neon.css') }}">

    @yield('style')
</head>
<body>

<div class="wrap">
    <!-- Logo and Navigation -->
    <div class="site-header-container container">
        <div class="row">
            <div class="col-md-12">
                <header class="site-header">
                    <section class="site-logo">
                        <a href="/">
                            <img src="{{ config('bfacp.community_logo', asset('frontend/images/logo@2x.png')) }}" width="120" />
                        </a>
                    </section>

                    <nav class="site-nav">
                        <ul class="main-menu hidden-xs" id="main-menu">
                            <li class="active">
                                <a href="{{ route('guest.home') }}">
                                    <span>Home</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('user.login') }}">
                                    <span>Login</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </header>
            </div>
        </div>
    </div>

    @yield('content')

</div>

<script src="{{ asset('frontend/js/jquery-1.11.3.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.0"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="{{ asset('frontend/js/bootstrap.js') }}"></script>
<script src="{{ asset('frontend/js/resizeable.js') }}"></script>
<script src="{{ asset('js/scripts/momentjs/moment-with-locales.js') }}"></script>

@yield('scripts')

</body>
</html>