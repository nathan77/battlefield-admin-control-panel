@extends('layouts.guest.master')

@section('title', 'Homepage')

@section('scripts')
    <script src="{{ asset('frontend/js/app.js') }}"></script>
@stop

@section('content')
    <section id="app">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <th scope="col">Server</th>
                        <th scope="col">Game</th>
                        <th scope="col">Players</th>
                        <th scope="col">Map</th>
                        <th scope="col">Mode</th>
                        </thead>

                        <tbody>
                        @foreach ($servers as $server)
                            <tr>
                                <td>{{ $server->ServerName }}</td>
                                <td>{{ $server->game->Name }}</td>
                                <td>{{ $server->usedSlots }} / {{ $server->maxSlots }}</td>
                                <td>{{ $server->mapName }}</td>
                                <td>{{ $server->Gamemode }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <th scope="col">ID</th>
                            <th scope="col">Player</th>
                            <th scope="col">Duration</th>
                            <th scope="col">Reason</th>
                        </thead>

                        <tbody>
                            @foreach ($latestBans as $ban)
                                <tr class="{{ $ban->color_class['frontend']['table'] }}">
                                    <td>{{ $ban->ban_id }}</td>
                                    <td>{{ $ban->player->SoldierName }}</td>
                                    <td>
                                        @if ($ban->is_perm)
                                            <span class="label {{ $ban->color_class['frontend']['label'] }}">
                                                Permanent
                                            </span>
                                        @else
                                            <span title="{{ $ban->ban_expires }}">
                                                @{{ moment("@php echo $ban->ban_expires @endphp").fromNow() }}
                                            </span>
                                        @endif
                                    </td>
                                    <td>{{ $ban->record->record_message }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            {{ $latestBans->links() }}
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- Features Blocks -->
    <section class="features-blocks">
        <div class="container">
            <div class="row vspace"><!-- "vspace" class is added to distinct this ro -->
                <div class="col-sm-6">
                    <div class="feature-block">
                        <h3>
                            <i class="entypo-gauge"></i>
                            Recently Seen Players
                        </h3>
                        <p>
                            On am we offices expense thought. Its hence ten smile age means. Seven chief sight far point any. Of so high into easy.
                        </p>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="feature-block">
                        <h3>
                            <i class="entypo-lifebuoy"></i>
                            24/7 Support
                        </h3>
                        <p>
                            Extremely eagerness principle estimable own was man. Men received far his dashwood subjects new.
                        </p>
                    </div>
                </div>
            </div>

            <!-- Separator -->
            <div class="row">
                <div class="col-md-12">
                    <hr />
                </div>
            </div>
        </div>
    </section>
@stop