<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\PermissionRegistrar;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        foreach (self::permissionsList() as $permission) {
            Permission::create($permission);
        }

        Role::create(['name' => 'Administrator']);
        $user = $this->createDefaultUser();
        $user->assignRole('Administrator');
    }

    /**
     * Creates the default administrator account.
     *
     * @return \App\User
     */
    private function createDefaultUser()
    {
        $user = new \App\User();
        $user->username = "admin";
        $user->display_name = "Administrator";
        $user->email = "example@example.com";
        $user->password = Hash::make("admin");
        $user->api_token = Str::random(60);
        $user->save();

        return $user;
    }

    /**
     * Application permissions
     *
     * @return array
     */
    public static function permissionsList()
    {
        $permissions = [
            [
                'name' => 'view.dashboard',
                'friendly_name' => 'View Dashboard',
                'description' => 'Allowed to view the dashboard',
            ],
            [
                'name' => 'view.player.profile',
                'friendly_name' => 'View Player Profile',
                'description' => 'Allowed to view the player profile',
            ],
            [
                'name' => 'view.chatlogs',
                'friendly_name' => 'View Chatlogs',
                'description' => 'Allowed to view the chat logs',
            ],
            [
                'name' => 'view.player.bans',
                'friendly_name' => 'View Player Bans',
                'description' => 'Allowed to view the player bans',
            ],
            [
                'name' => 'view.player.infractions',
                'friendly_name' => 'View Player Infractions',
                'description' => '',
            ],
            [
                'name' => 'view.player.ip',
                'friendly_name' => 'View Player IP',
                'description' => '',
            ],
            [
                'name' => 'view.player.eaguid',
                'friendly_name' => 'View Player EAGUID',
                'description' => '',
            ],
            [
                'name' => 'view.player.pbguid',
                'friendly_name' => 'View Player PBGUID',
                'description' => '',
            ],
            [
                'name' => 'view.player.history',
                'friendly_name' => 'View Player History',
                'description' => '',
            ],
        ];

        return $permissions;
    }
}
