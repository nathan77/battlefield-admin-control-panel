<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServerMetadataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('server_metadata', function (Blueprint $table) {
            $table->integer('server_id')->unsigned();
            $table->text('rcon_password')->nullable()->default(null);
            $table->integer('uptime_robot_monitor_key')->unsigned()->nullable()->default(null);
            $table->string('battlelog_guid', 100)->nullable()->default(null);
            $table->text('servername_filter')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('server_metadata');
    }
}
