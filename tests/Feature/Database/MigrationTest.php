<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/3/19, 6:50 AM
 */

namespace Tests\Database;

use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

/**
 * Class MigrationTest
 * @package Tests\Database
 */
class MigrationTest extends TestCase
{
    /**
     * Checks to make sure tables were created successfully.
     */
    public function testDatabaseForRequiredTables()
    {
        $tables = [
            'model_has_permissions',
            'failed_jobs',
            'migrations',
            'model_has_roles',
            'password_resets',
            'permissions',
            'player_ban_appeal',
            'roles',
            'role_has_permissions',
            'users',
        ];

        foreach ($tables as $table) {
            $this->assertTrue(Schema::hasTable($table), sprintf("'%s' table doesn't exist.", $table));
        }
    }

    /**
     * Tests to make sure all permissions got added.
     */
    public function testDatabaseHasPermissions()
    {
        $permissions = \RolesAndPermissionsSeeder::permissionsList();

        foreach ($permissions as $key => $perm) {
            $this->assertDatabaseHas('permissions', [
                'name' => $perm['name'],
            ]);
        }
    }

    /**
     * Tests to make sure the default admin role exists.
     */
    public function testDatabaseAdminRoleExists()
    {
        $this->assertDatabaseHas('roles', [
            'name' => 'Administrator',
            'guard_name' => 'web',
        ]);
    }

    /**
     * Checks to make sure the default admin account was created.
     */
    public function testDatabaseHasDefaultAdminUser()
    {
        // Does the account exist?
        $this->assertDatabaseHas('users', [
            'username' => 'admin',
            'email' => 'example@example.com',
        ]);

        // Does the account have the administrator role?
        $this->assertDatabaseHas('model_has_roles', [
            'role_id' => 1,
            'model_type' => 'App\User',
            'model_id' => 1,
        ]);
    }
}
