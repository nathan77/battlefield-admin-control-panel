<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/9/19, 2:10 AM
 */

namespace Tests\Feature\Routes;

use Tests\TestCase;

class GuestTest extends TestCase
{
    public function testGuestHomePage()
    {
        $this->markTestSkipped('Skipped guest homepage test');
        $this->assertGuest();

        $response = $this->get('/');
        $response->assertSuccessful()->assertViewIs('pages.guest.homepage');
    }

    public function testGuestHomePageToLoginPage()
    {
        $this->markTestSkipped('Skipped login page test');
        $this->assertGuest();

        $response = $this->get('/login');
        $response->assertSuccessful()->assertViewIs('auth.login');
    }
}
