<?php

namespace Tests\Feature\Routes;

use App\User;
use Tests\TestCase;

/**
 * Class AuthTest
 * @package Tests\Feature\Routes
 */
class AuthTest extends TestCase
{
    public function testDashboard()
    {
        $this->markTestSkipped('Skipped dashboard test');
        $user = User::where('username', 'admin')->first();
        $this->actingAs($user);
        $this->assertAuthenticated();

        $response = $this->get('/dashboard');
        $response->assertSuccessful()->assertViewIs('pages.dashboard');
    }
}
