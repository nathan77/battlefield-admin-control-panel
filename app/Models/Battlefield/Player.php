<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/3/19, 4:50 AM
 */

namespace App\Models\Battlefield;

use App\Models\Adkats\Ban;
use App\Models\Adkats\Battlelog;
use App\Models\Adkats\Infractions\Overall;
use App\Models\Adkats\Infractions\Server;
use App\Models\Adkats\Record;
use App\Models\Adkats\Special;
use App\Models\Elegant;
//use App\Models\Facades\Main as MainHelper;
use App\Models\Player\Dogtag;
use App\Models\Player\Session;
use App\Models\Player\Stat;
//use App\Models\Repositories\GeoRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache as Cache;

class Player extends Elegant
{
    /**
     * Should model handle timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'tbl_playerdata';

    /**
     * Table primary key.
     *
     * @var string
     */
    protected $primaryKey = 'PlayerID';

    /**
     * Fields not allowed to be mass assigned.
     *
     * @var array
     */
    protected $guarded = ['PlayerID'];

    /**
     * Date fields to convert to carbon instances.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * Append custom attributes to output.
     *
     * @var array
     */
    protected $appends = ['profile_url', 'country_flag', 'country_name', 'rank_image', 'links'];

    /**
     * Models to be loaded automatically.
     *
     * @var array
     */
    protected $with = ['game', 'battlelog'];

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dogtags()
    {
        return $this->hasMany(Dogtag::class, 'KillerID');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ban()
    {
        return $this->hasOne(Ban::class, 'player_id');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function stats()
    {
        return $this->hasManyThrough(Stat::class, 'App\Models\Player\Server', 'PlayerID', 'StatsID');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function sessions()
    {
        return $this->hasManyThrough(Session::class, 'App\Models\Player\Server', 'PlayerID', 'StatsID');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function infractionsGlobal()
    {
        return $this->hasOne(Overall::class, 'player_id');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function infractionsServer()
    {
        return $this->hasMany(Server::class, 'player_id');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function game()
    {
        return $this->belongsTo(Game::class, 'GameID');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function reputation()
    {
        return $this->hasOne(Reputation::class, 'player_id');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function recordsBy()
    {
        return $this->hasMany(Record::class, 'source_id');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function recordsOn()
    {
        return $this->hasMany(Record::class, 'target_id');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function battlelog()
    {
        return $this->hasOne(Battlelog::class, 'player_id');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function specialGroups()
    {
        return $this->hasMany(Special::class, 'player_id');
    }

    /**
     * Does the player have a battlelog persona id linked.
     *
     * @return bool
     */
    public function hasPersona()
    {
        return ! empty($this->battlelog);
    }

    /**
     * Checks if player has a reputation record.
     *
     * @return bool
     */
    public function hasReputation()
    {
        return ! empty($this->reputation);
    }

    /**
     * Purge the cache for the player.
     *
     * @return $this
     */
    public function forget()
    {
        Cache::forget(sprintf('api.player.%u', $this->PlayerID));
        Cache::forget(sprintf('player.%u', $this->PlayerID));

        return $this;
    }

    /**
     * Get the players aliases.
     *
     * @return array
     */
    public function aliases()
    {
        $aliases = Record::where('command_type', 48)->where('target_id',
            $this->PlayerID)->groupBy('record_message')->lists('record_message');

        return $aliases;
    }

    /**
     * Gets the URL to the players profile.
     *
     * @return string
     */
    public function getProfileUrlAttribute()
    {
//        return route('player.show', [
//            'id'   => $this->PlayerID,
//            'name' => $this->SoldierName,
//        ]);
    }

    /**
     * Get the country name.
     *
     * @return string
     */
    public function getCountryNameAttribute()
    {
        try {
            if ($this->CountryCode == '--' || empty($this->CountryCode)) {
                throw new Exception();
            }

//            $cc = MainHelper::countries($this->CountryCode);
//
//            if ($cc === null) {
//                throw new Exception();
//            }
//
//            return $cc;
        } catch (Exception $e) {
            return 'Unknown';
        }
    }

    /**
     * Get the country image flag.
     *
     * @return string
     */
    public function getCountryFlagAttribute()
    {
        try {
            if ($this->CountryCode == '--' || empty($this->CountryCode)) {
                throw new Exception();
            }

            $path = sprintf('images/flags/24/%s.png', strtoupper($this->CountryCode));

            if (! file_exists(sprintf('%s/%s', public_path(), $path))) {
                throw new Exception();
            }

            return $path;
        } catch (Exception $e) {
            return 'images/flags/24/_unknown.png';
        }
    }

    /**
     * Generates links to external/internal systems.
     *
     * @return array
     */
    public function getLinksAttribute()
    {
        switch ($this->game->Name) {
            case 'BFHL':
                $game = 'BFH';
                break;

            default:
                $game = $this->game->Name;
        }

        $links = [];

        // Battlelog URL
        if (is_null($this->battlelog)) {
            $links['battlelog'] = sprintf('http://battlelog.battlefield.com/%s/user/%s', strtolower($game),
                $this->SoldierName);
        } else {
            if ($game == 'BFH') {
                $links['battlelog'] = sprintf('http://battlelog.battlefield.com/%s/agent/%s/stats/%u/pc/',
                    strtolower($game), $this->SoldierName, $this->battlelog->persona_id);
            } else {
                $links['battlelog'] = sprintf('http://battlelog.battlefield.com/%s/soldier/%s/stats/%u/pc/',
                    strtolower($game), $this->SoldierName, $this->battlelog->persona_id);
            }
        }

        $links[] = [
            //'chatlogs' => route('chatlog.search', ['pid' => $this->PlayerID]),
            'pbbans'   => ! empty($this->PBGUID) ? sprintf('http://www.pbbans.com/mbi-guid-search-%s.html',
                $this->PBGUID) : null,
            'fairplay' => sprintf('https://www.247fairplay.com/CheatDetector/%s', $this->SoldierName),
        ];

        $links = array_merge($links, $links[0]);
        unset($links[0]);

        return $links;
    }

    /**
     * Get the rank image.
     *
     * @return string
     */
    public function getRankImageAttribute()
    {
        switch ($this->game->Name) {
            case 'BF3':
                $rank = $this->GlobalRank;

                if ($rank > 45) {
                    if ($rank > 100) {
                        $rank = 100;
                    }

                    $path = sprintf('images/games/bf3/ranks/large/ss%u.png', $rank);
                } else {
                    $path = sprintf('images/games/bf3/ranks/large/r%u.png', $this->GlobalRank);
                }
                break;

            case 'BF4':
                $path = sprintf('images/games/bf4/ranks/r%u.png', $this->GlobalRank);
                break;

            case 'BFHL':
                $path = sprintf('images/games/bfhl/ranks/r%u.png', $this->GlobalRank);
                break;

            default:
                $path = null;
        }

        return $path;
    }

    /**
     * Gets the geo data from ip address.
     *
     * @return array|void|null
     */
//    public function getGeoAttribute()
//    {
//        if (empty($this->IP_Address)) {
//            return;
//        }
//
//        try {
//            $geo = app(GeoRepository::class);
//
//            return $geo->set($this->IP_Address)->all();
//        } catch (Exception $e) {
//            return;
//        }
//    }
}
