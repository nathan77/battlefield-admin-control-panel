<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/3/19, 4:50 AM
 */

namespace App\Models\Battlefield;

use App\Models\Battlefield\Server\Server;
use App\Models\Elegant;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Game.
 * @property mixed Name
 */
class Game extends Elegant
{
    /**
     * Should model handle timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'tbl_games';

    /**
     * Table primary key.
     *
     * @var string
     */
    protected $primaryKey = 'GameID';

    /**
     * Fields not allowed to be mass assigned.
     *
     * @var array
     */
    protected $guarded = ['*'];

    /**
     * Date fields to convert to carbon instances.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * Append custom attributes to output.
     *
     * @var array
     */
    protected $appends = ['class_css'];

    /**
     * Models to be loaded automatically.
     *
     * @var array
     */
    protected $with = [];

    /**
     * @return HasMany
     */
    public function servers()
    {
        return $this->hasMany(Server::class, 'GameID');
    }

    /**
     * @return HasMany
     */
    public function reputations()
    {
        return $this->hasMany(Reputation::class, 'game_id');
    }

    /**
     * @return string
     */
    public function getClassCssAttribute()
    {
        switch ($this->Name) {
            case 'BF3':
                $class = 'label bg-purple';
                break;

            case 'BF4':
                $class = 'label bg-blue';
                break;

            case 'BFH':
            case 'BFHL':
                $class = 'label bg-green';
                break;

            default:
                $class = 'label bg-yellow';
        }

        return $class;
    }
}
