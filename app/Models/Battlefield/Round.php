<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/3/19, 4:50 AM
 */

namespace App\Models\Battlefield;

use App\Models\Battlefield\Server\Server;
use App\Models\Elegant;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Stats.
 */
class Round extends Elegant
{
    /**
     * Should model handle timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'tbl_extendedroundstats';

    /**
     * Table primary key.
     *
     * @var string
     */
    protected $primaryKey = 'server_id';

    /**
     * Fields not allowed to be mass assigned.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Date fields to convert to carbon instances.
     *
     * @var array
     */
    protected $dates = ['roundstat_time'];

    /**
     * Append custom attributes to output.
     *
     * @var array
     */
    protected $appends = [];

    /**
     * The attributes excluded form the models JSON response.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Models to be loaded automatically.
     *
     * @var array
     */
    protected $with = [];

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function server()
    {
        return $this->belongsTo(Server::class, 'ServerID');
    }

    /**
     * @param $query
     *
     * @return Model
     */
    public function scopeCurrent($query)
    {
        return $query->max('round_id');
    }

    /**
     * @param $query
     * @param $id
     *
     * @return Model
     */
    public function scopeRound($query, $id)
    {
        return $query->where('round_id', $id);
    }

    /**
     * @param        $query
     * @param Carbon $timeframe
     *
     * @return Model
     */
    public function scopeSince($query, Carbon $timeframe)
    {
        return $query->where('roundstat_time', '>=', $timeframe);
    }

    /**
     * @param $query
     *
     * @return Model
     */
    public function scopeBare($query)
    {
        return $query->groupBy('round_id')->selectRaw('round_id, MIN(roundstat_time) AS \'RoundStart\', MAX(roundstat_time) AS \'RoundEnd\'');
    }
}
