<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/3/19, 4:50 AM
 */

namespace App\Models\Battlefield\Server;

use App\Models\Battlefield\Game;
use App\Models\Battlefield\Round;
use App\Models\Battlefield\Scoreboard\Scoreboard;
use App\Models\Battlefield\Scoreboard\Scores;
use App\Models\Battlefield\Setting;
use App\Models\Elegant;
use App\Models\Facades\Battlefield as BattlefieldHelper;
use App\Models\Facades\Main as MainHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

/**
 * @property mixed maps_file_path
 * @property mixed port
 * @property mixed ip
 * @property mixed teams_file_path
 * @property mixed squads_file_path
 * @property mixed modes_file_path
 */
class Server extends Elegant
{
    /**
     * Should model handle timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'tbl_server';

    /**
     * Table primary key.
     *
     * @var string
     */
    protected $primaryKey = 'ServerID';

    /**
     * Fields not allowed to be mass assigned.
     *
     * @var array
     */
    protected $guarded = ['ServerID'];

    /**
     * Date fields to convert to carbon instances.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * Append custom attributes to output.
     *
     * @var array
     */
    protected $appends = [
        'percentage',
        'ip',
        'port',
        'server_name_short',
        //'in_queue',
        'maps_file_path',
        'modes_file_path',
        'squads_file_path',
        'teams_file_path',
        'current_map',
        'current_gamemode',
        'map_image_paths',
        'is_active',
        'slug',
    ];

    /**
     * The attributes excluded form the models JSON response.
     *
     * @var array
     */
    protected $hidden = ['maps_file_path', 'modes_file_path', 'squads_file_path', 'teams_file_path'];

    /**
     * Models to be loaded automatically.
     *
     * @var array
     */
    protected $with = ['game', 'setting'];

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function game()
    {
        return $this->belongsTo(Game::class, 'GameID');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function stats()
    {
        return $this->belongsTo(Stats::class, 'ServerID');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function maps()
    {
        return $this->hasMany(Maps::class, 'ServerID');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rounds()
    {
        return $this->hasMany(Round::class, 'server_id');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function scoreboard()
    {
        return $this->hasMany(Scoreboard::class, 'ServerID');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function setting()
    {
        return $this->hasOne(Setting::class, 'server_id');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function scores()
    {
        return $this->hasMany(Scores::class, 'ServerID');
    }

    /**
     * Only return servers that should be active.
     *
     * @param $query
     *
     * @return
     */
    public function scopeActive($query)
    {
        return $query->where('ConnectionState', 'on');
    }

    /**
     * Returns the server name with the strings that are
     * to be removed from it.
     *
     * @return string/null
     */
    public function getServerNameShortAttribute()
    {
        if (is_null($this->setting) || empty($this->setting->servername_filter)) {
            return null;
        }

        $strings = explode(',', $this->setting->servername_filter);

        return preg_replace('/\s+/', ' ', trim(str_replace($strings, null, $this->ServerName)));
    }

    /**
     * Calculates how full the server is represented by a percentage.
     *
     * @return float
     */
    public function getPercentageAttribute()
    {
        //return MainHelper::percent($this->usedSlots, $this->maxSlots);
    }

    /**
     * Gets the IP Address.
     *
     * @return string
     */
    public function getIPAttribute()
    {
        $host = explode(':', $this->IP_Address)[0];

        return gethostbyname($host);
    }

    /**
     * Gets the RCON port from the IP Address.
     *
     * @return int
     */
    public function getPortAttribute()
    {
        $port = explode(':', $this->IP_Address)[1];

        return (int) $port;
    }

    /**
     * Gets the human readable name of the current map.
     *
     * @return string
     */
    public function getCurrentMapAttribute()
    {
        //return BattlefieldHelper::mapName($this->mapName, $this->maps_file_path, $this->Gamemode);
    }

    /**
     * Gets the human readable name of the current mode.
     *
     * @return string
     */
    public function getCurrentGamemodeAttribute()
    {
        //return BattlefieldHelper::playmodeName($this->Gamemode, $this->modes_file_path);
    }

    /**
     * Gets the number of players currently in queue and caches the result for 5 minutes.
     *
     * @return int
     */
    public function getInQueueAttribute()
    {
        $result = Cache::remember('server.'.$this->ServerID.'.queue', 5, function () {
            $battlelog = App::make('App\Models\Libraries\Battlelog\BattlelogServer');

            return $battlelog->server($this)->inQueue();
        });

        return $result;
    }

    /**
     * Gets the path of the maps xml file.
     *
     * @return string
     */
    public function getMapsFilePathAttribute()
    {
        $path = app_path().DIRECTORY_SEPARATOR.'ThirdParty'.DIRECTORY_SEPARATOR.strtoupper($this->game->Name).DIRECTORY_SEPARATOR.'mapNames.xml';

        return $path;
    }

    /**
     * Gets the path of the gamemodes xml file.
     *
     * @return string
     */
    public function getModesFilePathAttribute()
    {
        $path = app_path().DIRECTORY_SEPARATOR.'ThirdParty'.DIRECTORY_SEPARATOR.strtoupper($this->game->Name).DIRECTORY_SEPARATOR.'playModes.xml';

        return $path;
    }

    /**
     * Gets the path of the squads xml file.
     *
     * @return string
     */
    public function getSquadsFilePathAttribute()
    {
        $path = app_path().DIRECTORY_SEPARATOR.'ThirdParty'.DIRECTORY_SEPARATOR.strtoupper($this->game->Name).DIRECTORY_SEPARATOR.'squadNames.xml';

        return $path;
    }

    /**
     * Gets the path of the teams xml file.
     *
     * @return string
     */
    public function getTeamsFilePathAttribute()
    {
        $path = app_path().DIRECTORY_SEPARATOR.'ThirdParty'.DIRECTORY_SEPARATOR.strtoupper($this->game->Name).DIRECTORY_SEPARATOR.'teamNames.xml';

        return $path;
    }

    /**
     * Gets the current map image banner.
     *
     * @return array|string
     */
    public function getMapImagePathsAttribute()
    {
        $base_path = sprintf('images/games/%s/maps', strtolower($this->game->Name));

        if ($this->game->Name == 'BFHL') {
            $image = sprintf('%s.png', strtolower($this->mapName));
        } else {
            $image = sprintf('%s.jpg', strtolower($this->mapName));
        }

        $paths = [
            'large'  => sprintf('%s/large/%s', $base_path, $image),
            'medium' => sprintf('%s/medium/%s', $base_path, $image),
            'wide'   => in_array($this->game->Name, ['BF4', 'BFHL']) ? sprintf('%s/wide/%s', $base_path,
                $image) : sprintf('%s/large/%s', $base_path, $image),
        ];

        return $paths;
    }

    /**
     * Is the server enabled?
     *
     * @return bool
     */
    public function getIsActiveAttribute()
    {
        return $this->ConnectionState == 'on';
    }

    /**
     * @return string
     */
    public function getSlugAttribute()
    {
        return Str::slug($this->ServerName);
    }
}
