<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/3/19, 4:50 AM
 */

namespace App\Models\Battlefield;

use App\Models\Elegant;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Reputation.
 * @property mixed total_rep_co
 * @property mixed total_rep_co
 * @property mixed total_rep_co
 * @property mixed total_rep_co
 */
class Reputation extends Elegant
{
    /**
     * Should model handle timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'adkats_player_reputation';

    /**
     * Table primary key.
     *
     * @var string
     */
    protected $primaryKey = 'player_id';

    /**
     * Fields allowed to be mass assigned.
     *
     * @var array
     */
    protected $guarded = ['player_id'];

    /**
     * Date fields to convert to carbon instances.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * Append custom attributes to output.
     *
     * @var array
     */
    protected $appends = ['color'];

    /**
     * Models to be loaded automatically.
     *
     * @var array
     */
    protected $with = [];

    /**
     * @param $query
     * @param $game
     *
     * @return mixed
     */
    public function scopeOfGame($query, $game)
    {
        return $query->whereHas('game', function ($q) use ($game) {
            return $q->where('Name', $game);
        });
    }

    /**
     * Gets the class name for the view.
     *
     * @return string
     */
    public function getColorAttribute()
    {
        $color = 'text-blue';

        if ($this->total_rep_co > 100) {
            $color = 'text-green';
        } elseif ($this->total_rep_co < -100) {
            $color = 'text-red';
        } elseif ($this->total_rep_co >= -100 && $this->total_rep_co <= 100) {
            $color = 'text-yellow';
        }

        return $color;
    }

    /**
     * @param $query
     *
     * @return Model
     */
    public function scopeMostReputable($query)
    {
        return $query->where('total_rep_co', '>=', 300)->orderBy('total_rep_co', 'desc');
    }

    /**
     * @param $query
     *
     * @return Model
     */
    public function scopeLeastReputable($query)
    {
        return $query->where('total_rep_co', '<=', -300)->orderBy('total_rep_co');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function player()
    {
        return $this->belongsTo('App\Models\Battlefield\Player', 'player_id');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function game()
    {
        return $this->belongsTo('App\Models\Battlefield\Game', 'game_id');
    }
}
