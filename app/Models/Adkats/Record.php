<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/3/19, 4:50 AM
 */

namespace App\Models\Adkats;

use App\Models\Battlefield\Player;
use App\Models\Battlefield\Server\Server;
use App\Models\Elegant;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Record.
 *
 * @property int      record_id
 * @property int      server_id
 * @property int      command_type
 * @property int      command_action
 * @property int      command_numeric
 * @property string   target_name
 * @property int|null target_id
 * @property string   source_name
 * @property int|null source_id
 * @property string   record_message
 * @property string   record_time
 * @property string   adkats_read
 * @property bool     adkats_web
 */
class Record extends Elegant
{
    /**
     * Use mysql2 connection to disable prefix
     *
     * @var string
     */
    //protected $connection = 'mysql2';

    /**
     * Should model handle timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'adkats_records_main';

    /**
     * Table primary key.
     *
     * @var string
     */
    protected $primaryKey = 'record_id';

    /**
     * Fields not allowed to be mass assigned.
     *
     * @var array
     */
    protected $guarded = ['record_id'];

    /**
     * Date fields to convert to carbon instances.
     *
     * @var array
     */
    protected $dates = ['record_time'];

    /**
     * Append custom attributes to output.
     *
     * @var array
     */
    protected $appends = ['is_web', 'stamp'];

    /**
     * Models to be loaded automatically.
     *
     * @var array
     */
    protected $with = [];

    /**
     * @return BelongsTo
     */
    public function target()
    {
        return $this->belongsTo(Player::class, 'target_id');
    }

    /**
     * @return BelongsTo
     */
    public function source()
    {
        return $this->belongsTo(Player::class, 'source_id');
    }

    /**
     * @return BelongsTo
     */
    public function server()
    {
        return $this->belongsTo(Server::class, 'server_id');
    }

    /**
     * @return BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(Command::class, 'command_type')->select([
            'command_id',
            'command_active',
            'command_name',
            'command_playerInteraction',
        ]);
    }

    /**
     * @return BelongsTo
     */
    public function action()
    {
        return $this->belongsTo(Command::class, 'command_action')->select([
            'command_id',
            'command_active',
            'command_name',
            'command_playerInteraction',
        ]);
    }

    /**
     * Was record issued from the web.
     *
     * @return bool
     */
    public function getIsWebAttribute()
    {
        return $this->attributes['adkats_web'] == 1;
    }

    /**
     * @return mixed
     */
    public function getStampAttribute()
    {
        return $this->record_time->toIso8601String();
    }
}
