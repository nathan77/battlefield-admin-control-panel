<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/3/19, 4:50 AM
 */

namespace App\Models\Adkats\Account;

use App\Models\Adkats\Command;
use App\Models\Elegant;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Role.
 */
class Role extends Elegant
{
    /**
     * Should model handle timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'adkats_roles';

    /**
     * Table primary key.
     *
     * @var string
     */
    protected $primaryKey = 'role_id';

    /**
     * Fields allowed to be mass assigned.
     *
     * @var array
     */
    protected $guarded = ['role_id'];

    /**
     * Date fields to convert to carbon instances.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * Append custom attributes to output.
     *
     * @var array
     */
    protected $appends = [];

    /**
     * Models to be loaded automatically.
     *
     * @var array
     */
    protected $with = [];

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class, 'user_role');
    }

    /**
     * Return the power level of the role.
     *
     * @param $count
     *
     * @return int
     */
    public function getPowerLevel($count)
    {
        return ($count) + (2 * $this->permissions()->admin()->count());
    }

    /**
     * Trims $value and sets the role key.
     *
     * @param $value
     */
    public function setRoleNameAttribute($value)
    {
        $name = trim($value);
        $this->attributes['role_name'] = $name;
        $this->attributes['role_key'] = strtolower(str_replace(' ', '_', $name));
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany(Command::class, 'adkats_rolecommands', 'role_id', 'command_id');
    }
}
