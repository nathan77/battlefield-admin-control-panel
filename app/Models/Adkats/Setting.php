<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/3/19, 4:50 AM
 */

namespace App\Models\Adkats;

use App\Models\Battlefield\Server\Server;
use App\Models\Elegant;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Setting.
 * @property mixed setting_type
 */
class Setting extends Elegant
{
    /**
     * Should model handle timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'adkats_settings';

    /**
     * Table primary key.
     *
     * @var string
     */
    protected $primaryKey = 'server_id';

    /**
     * Fields not allowed to be mass assigned.
     *
     * @var array
     */
    protected $fillable = ['setting_value'];

    /**
     * Date fields to convert to carbon instances.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * Append custom attributes to output.
     *
     * @var array
     */
    protected $appends = [];

    /**
     * Models to be loaded automatically.
     *
     * @var array
     */
    protected $with = [];

    /**
     * @return BelongsTo
     */
    public function server()
    {
        return $this->belongsTo(Server::class, 'server_id');
    }

    /**
     * Quick way of selecting specific commands.
     *
     * @param              $query
     * @param array|string $names Command Names
     *
     * @return
     */
    public function scopeSettings($query, $names)
    {
        if (is_string($names)) {
            return $query->where('setting_name', $names);
        }

        return $query->whereIn('setting_name', $names);
    }

    /**
     * Quick way of selecting servers.
     *
     * @param       $query
     * @param array $ids Array of server ids
     *
     * @return
     */
    public function scopeServers($query, $ids)
    {
        if (is_numeric($ids)) {
            return $query->where('server_id', $ids);
        }

        return $query->whereIn('server_id', $ids);
    }

    /**
     * Convert value to correct type.
     *
     * @return mixed
     */
    public function getSettingValueAttribute()
    {
        $value = $this->attributes['setting_value'];
        $settingName = $this->attributes['setting_name'];

        if (! array_key_exists('setting_name', $this->attributes) || $settingName == 'Custom HTML Addition') {
            return $value;
        }

        switch ($this->setting_type) {
            case 'multiline':
                if (in_array($settingName, [
                    'Pre-Message List',
                    'Server Rule List',
                    'SpamBot Say List',
                    'SpamBot Yell List',
                ])) {
                    $value = rawurldecode(urldecode($value));
                }

                if (strlen($value) == 0) {
                    return $value;
                }

                $valueArray = explode('|', $value);

                return $valueArray;
                break;

            case 'bool':
                return $value == 'True';
                break;

            case 'int':
                return (int) $value;
                break;

            case 'double':
                return (float) $value;
                break;

            case 'stringarray':
                return explode('|', $value);
                break;

            default:
                return $value;
        }
    }
}
