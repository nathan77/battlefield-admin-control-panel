<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/4/19, 1:11 AM
 */

namespace App\Models\Adkats;

use App\Models\Battlefield\Player;
use App\Models\Elegant;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Ban.
 * @property mixed record
 * @property mixed ban_endTime
 * @property mixed ban_startTime
 */
class Ban extends Elegant
{
    /**
     * Should model handle timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'adkats_bans';

    /**
     * Table primary key.
     *
     * @var string
     */
    protected $primaryKey = 'ban_id';

    /**
     * Fields not allowed to be mass assigned.
     *
     * @var array
     */
    protected $guarded = ['ban_id', 'ban_sync'];

    /**
     * Date fields to convert to carbon instances.
     *
     * @var array
     */
    protected $dates = ['ban_startTime', 'ban_endTime'];

    /**
     * Append custom attributes to output.
     *
     * @var array
     */
    protected $appends = [
        'is_active',
        'is_expired',
        'is_unbanned',
        'is_perm',
        'ban_enforceName',
        'ban_enforceGUID',
        'ban_enforceIP',
        'ban_issued',
        'ban_expires',
        'color_class',
    ];

    /**
     * Models to be loaded automatically.
     *
     * @var array
     */
    protected $with = [];

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function record()
    {
        return $this->belongsTo(Record::class, 'latest_record_id');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function player()
    {
        return $this->belongsTo(Player::class, 'player_id');
    }

    /**
     * Gets the latest bans that are in effect.
     *
     * @param object $query
     *
     * @return Model
     */
    public function scopeLatest($query)
    {
        return $query->orderBy('ban_startTime', 'desc');
    }

    /**
     * Only retrieve active bans.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('ban_status', 'Active');
    }

    /**
     * Gets the bans done yesterday (UTC).
     *
     * @param object $query
     *
     * @return Model
     */
    public function scopeYesterday($query)
    {
        return $query->where('ban_startTime', '>=', Carbon::yesterday())->where('ban_startTime', '<=', Carbon::today());
    }

    /**
     * Get the bans that the player ids have done.
     *
     * @param object $query
     * @param array  $playerIds
     * @param int    $limit
     *
     * @return Model
     */
    public function scopePersonal($query, $playerIds = [], $limit = 30)
    {
        if (empty($playerIds)) {
            return $this;
        }

        return $query->join('adkats_records_main', 'adkats_bans.latest_record_id', '=',
            'adkats_records_main.record_id')->whereIn('adkats_records_main.source_id',
            $playerIds)->orderBy('ban_startTime', 'desc')->take($limit);
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function previous()
    {
        return $this->hasMany(Record::class, 'target_id', 'player_id')->whereIn('command_action',
            [7, 8, 72, 73])->orderBy('record_time', 'desc');
    }

    /**
     * @return mixed
     */
    public function getBanIssuedAttribute()
    {
        return $this->ban_startTime->toIso8601String();
    }

    /**
     * @return mixed
     */
    public function getBanExpiresAttribute()
    {
        return $this->ban_endTime->toIso8601String();
    }

    /**
     * Is ban enforced by name.
     *
     * @return bool
     */
    public function getBanEnforceNameAttribute()
    {
        return $this->attributes['ban_enforceName'] == 'Y';
    }

    /**
     * IS ban enforced by guid.
     *
     * @return bool
     */
    public function getBanEnforceGUIDAttribute()
    {
        return $this->attributes['ban_enforceGUID'] == 'Y';
    }

    /**
     * Is ban enforced by ip.
     *
     * @return bool
     */
    public function getBanEnforceIPAttribute()
    {
        return $this->attributes['ban_enforceIP'] == 'Y';
    }

    /**
     * Is ban active.
     *
     * @return bool
     */
    public function getIsActiveAttribute()
    {
        return $this->attributes['ban_status'] == 'Active';
    }

    /**
     * Is ban expired.
     *
     * @return bool
     */
    public function getIsExpiredAttribute()
    {
        return $this->attributes['ban_status'] == 'Expired';
    }

    /**
     * Is unbanned.
     *
     * @return bool
     */
    public function getIsUnbannedAttribute()
    {
        if (array_key_exists('ban_status', $this->attributes)) {
            return $this->attributes['ban_status'] == 'Disabled' || $this->attributes['ban_status'] == 'Expired';
        }
    }

    /**
     * Is ban permanent?
     *
     * @return bool
     */
    public function getIsPermAttribute()
    {
        return $this->record->command_action == 8;
    }

    public function getColorClassAttribute()
    {
        $data = [
            'frontend' => [
                'table' => '',
                'text' => '',
                'label' => '',
            ],
        ];

        if ($this->is_perm) {
            $data['frontend']['table'] = 'danger';
            $data['frontend']['text'] = 'warning';
            $data['frontend']['label'] = 'label-danger';
        }

        return $data;
    }
}
