<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/3/19, 4:50 AM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;

/**
 * Class Elegant.
 */
class Elegant extends Model
{
    /**
     * Use mysql2 connection to disable prefix
     *
     * @var string
     */
    protected $connection = 'mysql2';

    /**
     * Validation rules.
     *
     * @var array
     */
    protected static $rules = [];

    /**
     * Custom messages.
     *
     * @var array
     */
    protected static $messages = [];

    /**
     * Validation errors.
     *
     * @var MessageBag
     */
    protected $errors = [];

    /**
     * Validator instance.
     *
     * @var Validator
     */
    protected $validator;

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * Listen for save event.
     */
    protected static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            return $model->validate();
        });
    }

    /**
     * Validates current attributes against rules.
     */
    public function validate()
    {
        $v = Validator::make($this->attributes, static::$rules, static::$messages);

        if ($v->fails()) {
            $this->setErrors($v->messages());

            return false;
        }

        return true;
    }

    /**
     * Retrieve error message bag.
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Set error message bag.
     *
     * @var MessageBag
     */
    protected function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * Inverse of wasSaved.
     */
    public function hasErrors()
    {
        return ! empty($this->errors);
    }

    /**
     * Retrieve the validation rules.
     */
    public function getRules()
    {
        return static::$rules;
    }
}
