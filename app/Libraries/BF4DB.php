<?php


namespace App\Libraries;


use App\Models\Battlefield\Player;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class BF4DB
 * @package App\Libraries
 */
class BF4DB
{
    /**
     * BF4DB API Base URL
     */
    const API_URL = "https://bf4db.com/api/";

    /**
     * BF4DB constructor.
     * @throws Exception
     */
    public function __construct()
    {
        if (is_null(config('services.bf4db.api_key'))) {
            throw new Exception("BF4DB API Key Not Set");
        }
    }

    /**
     * @param Player $player
     * @param string $description
     * @param string $request_url
     * @param string $submitted_by
     * @param array $battlelog_urls
     * @param array $youtube_urls
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function submitEvidence(
        Player $player,
        string $description,
        string $request_url,
        string $submitted_by,
        array $battlelog_urls,
        array $youtube_urls
    )
    {
        return $this->request(
            'POST',
            'player/' . $player->battlelog->persona_id . '/evidence',
            [
                'description' => $description,
                'request_url' => $request_url,
                'submitted_by' => $submitted_by,
                'battlelog_urls' => $battlelog_urls,
                'youtube_urls' => $youtube_urls,
            ]
        );
    }

    /**
     * @param Player $player
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws GuzzleException
     */
    public function getPlayer(Player $player)
    {
        return $this->request('GET', 'player/' . $player->battlelog->persona_id);
    }

    /**
     * @param $method
     * @param $uri
     * @param array $params
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    private function request($method, $uri, $params = [])
    {
        $client = new Client([
            'base_uri' => self::API_URL,
            'timeout' => 5.0,
        ]);

        try {
            $request = $client->request($method, $uri, [
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => 'Bearer ' . env('BF4DB_API_KEY'),
                    'Accept-Encoding' => 'gzip',
                ],
                'decode_content' => true,
                'query' => $params,
            ]);

            // Decode the response so we can send it back to the browser as a JSON response
            $decode = json_decode($request->getBody());

            return response()->json($decode);
        } catch (GuzzleException $e) {
            throw $e;
        }
    }
}
