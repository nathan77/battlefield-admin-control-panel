<?php

namespace App\Console\Commands;

use App\Models\Adkats\Infractions\Overall;
use Illuminate\Console\Command;

class InfractionsCleanup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bfacp:infractions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleans up infractions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Overall::with('player', 'servers')->where('total_points', '<', 0)->chunk(1000, function ($infractions) {
            foreach ($infractions as $infraction) {
                $total = abs($infraction->total_points);
                if ($infraction->forgive_points > 0 && $infraction->punish_points == 0) {
                    $total = $infraction->forgive_points;
                } else if ($infraction->forgive_points > $infraction->punish_points) {
                    $total = $infraction->forgive_points - $infraction->punish_points;
                }
                $this->info(sprintf('Deleted %s forgives for %s', $total, $infraction->player->SoldierName));
                sleep(0.5);
            }
        });
    }
}
