<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/4/19, 1:21 AM
 */

namespace App\Repository\Adkats;

use App\Models\Adkats\Ban;
use App\Repository\Base;

class Bans extends Base
{
    public function latest($limit = 30)
    {
        $bans = Ban::with('player', 'record')
            //->active()
            ->orderBy('ban_startTime', 'desc')
            ->paginate(30);

        return $bans;
    }
}