<?php
/**
 * BFACP - Battlefield Admin Control Panel - Development by Prophet731 - Version 3.0
 *
 * BFACP was inspired by the gaming community A Different Kind (ADK). Visit http://www.ADKGamers.com/ for more information.
 *
 * Copyright 2014-2019. A Different Kind, LLC
 *
 * Project Source: https://gitlab.com/Prophet731/battlefield-admin-control-panel
 * LICENSE: GNU General Public License v3.0
 *
 * Last Modified: 11/3/19, 11:34 PM
 */

namespace App\Http\Controllers\Guest;

use App\Http\Controllers\Controller;
use App\Models\Battlefield\Server\Server;
use App\Repository\Adkats\Bans;

class LandingController extends Controller
{
    public function __invoke()
    {
        $latestBans = (new Bans())->latest();

        $servers = (new Server())->active()->get();
        return view('pages.guest.homepage', compact('latestBans', 'servers'));
    }
}