<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::namespace('Guest')->group(function() {
    Route::group(['middleware' => 'guest'], function () {
        Route::get('/', 'LandingController')->name('guest.home');
    });
});

Route::namespace('Auth')->group(function () {
    Route::get('login', 'LoginController@displayLogin')->name('user.login');
    Route::post('login', 'LoginController@login')->name('user.login.post');
    Route::get('logout', 'LoginController@logout')->name('user.logout');
});

// Route url
Route::get('dashboard', 'DashboardController@dashboardAnalytics')->middleware('auth')->name('dashboard');

// Route Dashboards
Route::get('/dashboard-analytics', 'DashboardController@dashboardAnalytics');

// Route Components
Route::get('/sk-layout-2-columns', 'StaterkitController@columns_2');
Route::get('/sk-layout-fixed-navbar', 'StaterkitController@fixed_navbar');
Route::get('/sk-layout-floating-navbar', 'StaterkitController@floating_navbar');
Route::get('/sk-layout-fixed', 'StaterkitController@fixed_layout');
